﻿using QuickGraph;

namespace GUI.Assets
{ 
    public class Graph
    {
        public string Name { get; set; }
        public IBidirectionalGraph<object, IEdge<object>> GraphVisual { get; set; }
    }
}