﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using QuickGraph;
using System.Diagnostics;
using GUI.Assets;
using PropertyChanged;

namespace GUI.ViewModel
{
    [ImplementPropertyChanged]
    public class MainViewModel : IMainViewModel
    {
        public ICommand AddRandomVertexCommand { get; set; }
        public ObservableCollection<Graph> GraphsList { get; set; }
        public string Description { get; set; }
        public IBidirectionalGraph<object, IEdge<object>> GraphVisualView { get; set; }
        public bool AddEdgeSelected { get; set; }
        public bool DeleteSelected { get; set; }
        public bool DeleteEdgeSelected { get; set; }
        public bool MoveVertexSelected { get; set; }
        public bool AddVertexSelected { get; set; }
        public bool ShowAbout { get; set; }

        public MainViewModel()
        {
            AddRandomVertexCommand = new RelayCommand(AddRandomVertexMethod);
            MoveVertexSelected = true;
            GraphVisualView = new BidirectionalGraph<object, IEdge<object>>();
        }

        public void AddRandomVertexMethod()
        {
            try
            {
                var g = (BidirectionalGraph<object, IEdge<object>>) GraphVisualView;
                var vertex = !g.Vertices.Any() ? 0 : g.Vertices.Max(x => ((int) x));
                g.AddVertex(++vertex);

                var size = g.Vertices.Count();
                if (size > 0)
                {
                    var randomVertex = g.Vertices.ElementAt(new Random().Next(0, size - 1));
                    g.AddEdge(new Edge<object>(vertex, randomVertex));
                }
            }
            catch (Exception exception)
            {
                Debug.Print(exception.Message);
            }
        }
    }
}
