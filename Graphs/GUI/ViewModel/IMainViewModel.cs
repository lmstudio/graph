using System.Collections.ObjectModel;
using System.Windows.Input;
using GUI.Assets;
using QuickGraph;

namespace GUI.ViewModel
{
    public interface IMainViewModel
    {
        IBidirectionalGraph<object, IEdge<object>> GraphVisualView { get; set; }
        ICommand AddRandomVertexCommand { get; set; }
        ObservableCollection<Graph> GraphsList { get; set; }
        string Description { get; set; }

        bool AddEdgeSelected { get; set; }
        bool DeleteSelected { get; set; }
        bool MoveVertexSelected { get; set; }
        bool AddVertexSelected { get; set; }
        bool ShowAbout { get; set; }

        void AddRandomVertexMethod();
    }
}