﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GUI.Assets;
using QuickGraph;

namespace GUI.ViewModel
{
    public class DesignTimeViewModel : IMainViewModel
    {
        public IBidirectionalGraph<object, IEdge<object>> GraphVisualView { get; set; }
        public ICommand AddRandomVertexCommand { get; set; }
        public ObservableCollection<Graph> GraphsList { get; set; }
        public string Description { get; set; }
        public bool AddEdgeSelected { get; set; }
        public bool DeleteSelected { get; set; }
        public bool DeleteEdgeSelected { get; set; }
        public bool MoveVertexSelected { get; set; }
        public bool AddVertexSelected { get; set; }
        public bool ShowAbout { get; set; }


        public DesignTimeViewModel()
        {
            var g = new BidirectionalGraph<object, IEdge<object>>(true);

            MoveVertexSelected = true;
            ShowAbout = true;

            Enumerable.Range(1, 5).ToList().ForEach(v => g.AddVertex(v));

            //add some edges to the graph
            g.AddEdge(new Edge<object>(4, 5));
            g.AddEdge(new Edge<object>(5, 4));
            g.AddEdge(new Edge<object>(1, 3));
            g.AddEdge(new Edge<object>(2, 5));
            g.AddEdge(new Edge<object>(1, 5));

            GraphVisualView = g;
            Description = @"Mason Graph v1.3
This software is distributed on GPL3 License, It's free to use, redistribute and modify. It's provided 'as it is' without any warranty.
Visit https://www.gnu.org/licenses/gpl-3.0.txt";
        }
        public void AddRandomVertexMethod()
        {
            throw new System.NotImplementedException();
        }


        
    }
}