﻿using System;
using System.Threading.Tasks;

namespace GUI
{
    public interface IFramework
    {
        Task RunInBackground(Action action);
    }

    public class Framework : IFramework
    {
        public async Task RunInBackground(Action action)
        {
            var task = Task.Run(action);
            await task;
        } 
    }
}