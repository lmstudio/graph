﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GraphSharp.Controls;
using GUI.Assets;
using GUI.ViewModel;
using QuickGraph;

namespace GUI
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IMainViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new MainViewModel
            {
                GraphsList = new ObservableCollection<Graph>()
            }; //IoC pełną gębą...
            DataContext = _viewModel;

            _viewModel.Description = @"Mason Graph v1.3
This software is distributed on GPL3 License, It's free to use, redistribute and modify. It's provided 'as it is' without any warranty.
Visit https://www.gnu.org/licenses/gpl-3.0.txt";

            AddGraphs();
        }

        private object _firstVertex = null;
        private object _secondVertex = null;
        private void VertexClicked(object sender, MouseButtonEventArgs e)
        {
            var vertex = ((VertexControl) sender).Vertex;
            Debug.Print(vertex.ToString());

            if (_viewModel.DeleteSelected)
            {
                ((BidirectionalGraph<object, IEdge<object>>) _viewModel.GraphVisualView).RemoveVertex(vertex);
            }

            else if (_viewModel.AddEdgeSelected)
            {
                try
                {
                    if (_firstVertex == null)
                    {
                        _firstVertex = vertex;
                        return;
                    }
                    if (_secondVertex == null)
                    {
                        _secondVertex = vertex;
                        ((BidirectionalGraph<object, IEdge<object>>) _viewModel.GraphVisualView)
                            .AddEdge(new Edge<object>(_firstVertex, _secondVertex));
                        _firstVertex = null;
                        _secondVertex = null;
                    }
                }
                catch
                {
                    _firstVertex = null;
                    _secondVertex = null;
                }


            }
        }

        public void EdgeClicked(object sender, MouseButtonEventArgs e)
        {
            if (_viewModel.DeleteSelected)
            {
                var edge = ((EdgeControl) sender).Edge;
                ((BidirectionalGraph<object, IEdge<object>>)_viewModel.GraphVisualView)
                        .RemoveEdge((IEdge<object>) edge);
            }
        }

        private void AddGraphs()
        {
            var g1 = new BidirectionalGraph<object, IEdge<object>>(true);
            Enumerable.Range(1, 5).ToList().ForEach(v => g1.AddVertex(v));
            g1.AddEdge(new Edge<object>(1, 2));
            g1.AddEdge(new Edge<object>(1, 3));
            g1.AddEdge(new Edge<object>(1, 4));
            g1.AddEdge(new Edge<object>(1, 5));
            g1.AddEdge(new Edge<object>(2, 1));
            g1.AddEdge(new Edge<object>(2, 3));
            g1.AddEdge(new Edge<object>(2, 4));
            g1.AddEdge(new Edge<object>(2, 5));
            g1.AddEdge(new Edge<object>(3, 1));
            g1.AddEdge(new Edge<object>(3, 2));
            g1.AddEdge(new Edge<object>(3, 4));
            g1.AddEdge(new Edge<object>(3, 5));
            g1.AddEdge(new Edge<object>(4, 1));
            g1.AddEdge(new Edge<object>(4, 2));
            g1.AddEdge(new Edge<object>(4, 3));
            g1.AddEdge(new Edge<object>(4, 5));
            g1.AddEdge(new Edge<object>(5, 1));
            g1.AddEdge(new Edge<object>(5, 2));
            g1.AddEdge(new Edge<object>(5, 3));
            g1.AddEdge(new Edge<object>(5, 4));
            _viewModel.GraphsList.Add(new Graph
            {
                Name = "Kuratowski",
                GraphVisual = g1
            });

            var g2 = new BidirectionalGraph<object, IEdge<object>>(true);
            Enumerable.Range(1, 3).ToList().ForEach(v => g2.AddVertex(v));
            g2.AddEdge(new Edge<object>(1, 2));
            g2.AddEdge(new Edge<object>(1, 3));
            g2.AddEdge(new Edge<object>(2, 1));
            g2.AddEdge(new Edge<object>(2, 3));
            g2.AddEdge(new Edge<object>(3, 1));
            g2.AddEdge(new Edge<object>(3, 2));
            _viewModel.GraphsList.Add(new Graph
            {
                Name = "K3",
                GraphVisual = g2
            });

            var g3 = new BidirectionalGraph<object, IEdge<object>>(true);
            Enumerable.Range(1, 5).ToList().ForEach(v => g3.AddVertex(v));
            g3.AddEdge(new Edge<object>(1, 3));
            g3.AddEdge(new Edge<object>(1, 4));
            g3.AddEdge(new Edge<object>(1, 5));
            g3.AddEdge(new Edge<object>(2, 3));
            g3.AddEdge(new Edge<object>(2, 4));
            g3.AddEdge(new Edge<object>(2, 5));
            g3.AddEdge(new Edge<object>(3, 1));
            g3.AddEdge(new Edge<object>(3, 2));
            g3.AddEdge(new Edge<object>(4, 1));
            g3.AddEdge(new Edge<object>(4, 2));
            g3.AddEdge(new Edge<object>(5, 1));
            g3.AddEdge(new Edge<object>(5, 2));
            _viewModel.GraphsList.Add(new Graph
            {
                Name = "K2,3",
                GraphVisual = g3
            });

            var g4 = new BidirectionalGraph<object, IEdge<object>>(true);
            Enumerable.Range(1, 4).ToList().ForEach(v => g4.AddVertex(v));
            g4.AddEdge(new Edge<object>(1, 2));
            g4.AddEdge(new Edge<object>(1, 3));
            g4.AddEdge(new Edge<object>(1, 4));
            g4.AddEdge(new Edge<object>(2, 1));
            g4.AddEdge(new Edge<object>(2, 3));
            g4.AddEdge(new Edge<object>(2, 4));
            g4.AddEdge(new Edge<object>(3, 1));
            g4.AddEdge(new Edge<object>(3, 2));
            g4.AddEdge(new Edge<object>(3, 4));
            g4.AddEdge(new Edge<object>(4, 1));
            g4.AddEdge(new Edge<object>(4, 2));
            g4.AddEdge(new Edge<object>(4, 3));
            _viewModel.GraphsList.Add(new Graph
            {
                Name = "K4",
                GraphVisual = g4
            });
        }

        private void SelectedGraph(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox) sender;
            var graph = (Graph)comboBox.SelectedItem;
            _viewModel.GraphVisualView = graph.GraphVisual;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ShowAbout = !_viewModel.ShowAbout;
        }

        private void AthLogo_Clicked(object sender, RoutedEventArgs e)
        {
            Process.Start(@"http://www.info.ath.bielsko.pl/");
        }
    }
}
