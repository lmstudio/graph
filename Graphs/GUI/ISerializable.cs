﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xaml;

namespace GUI
{
    public interface ISerializable
    {
        void Save<T>(string path, T obj);
        T Load<T>(string path);
    }
    public class Serializable : ISerializable
    {
        public void Save<T>(string path,T obj)
            =>XamlServices.Save(path,obj);

        public T Load<T>(string path)
            => (T)XamlServices.Load(path);
    }
}
